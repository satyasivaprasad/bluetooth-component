package com.m.myblechatsample

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.NightMode
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.m.myblechatsample.di.applicationModule
import com.m.myblechatsample.di.viewModule
import com.realwear.bluetooth.di.bluetoothApplicationModule
import com.realwear.bluetooth.di.bluetoothConnectionModule
import com.realwear.bluetooth.model.BluetoothConnector
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.startKoin
import org.koin.core.scope.Scope

class ChatApplication : Application(), LifecycleObserver {

    var isConversationsOpened = false
    var currentChat: String? = null


    @NightMode
    private var nightMode: Int = AppCompatDelegate.MODE_NIGHT_NO


    private lateinit var localeSession: Scope

    override fun onCreate() {
        super.onCreate()

        startKoin(
            this, listOf(
                applicationModule,
                bluetoothApplicationModule,
                bluetoothConnectionModule,
                viewModule
            )
        )

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

    }

    private val connector: BluetoothConnector by inject()

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    internal fun prepareConnection() {
        connector.prepare()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    internal fun releaseConnection() {
        connector.release()
    }
}
