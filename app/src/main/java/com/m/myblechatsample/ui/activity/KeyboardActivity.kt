package com.m.myblechatsample.ui.activity

import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.widget.Toast
import com.m.myblechatsample.R
import com.m.myblechatsample.ui.presenter.KeyboardPresenter
import com.m.myblechatsample.ui.view.BTKeyboardView
import kotlinx.android.synthetic.main.keyboard_activity.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class KeyboardActivity : SkeletonActivity(), BTKeyboardView {

    val keyboardPresenter: KeyboardPresenter by inject {
        parametersOf(getBluetoothDevice(), this)
    }

    private fun getBluetoothDevice(): String? {
        return intent.extras?.getParcelable<BluetoothDevice>(ScanActivity.EXTRA_BLUETOOTH_DEVICE)?.address
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.keyboard_activity)
        lifecycle.addObserver(keyboardPresenter)

        send_button.setOnClickListener {
            keyboardPresenter.sendTextMessage(input_text.text.toString());
        }

    }

    override fun showBluetoothDisabled() {
        Toast.makeText(this, "Bluetooth is disabled", Toast.LENGTH_SHORT).show()
    }

    override fun showReceivedMessage(message: String) {
        Toast.makeText(this, "Message Received " + message, Toast.LENGTH_SHORT).show()
    }

    override fun showSentMessage(message: String) {
        Toast.makeText(this, "Message sent :: " + message, Toast.LENGTH_SHORT).show()
    }

    override fun goBack() {
        finish()
    }

}
