package com.m.myblechatsample.ui.presenter

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.m.myblechatsample.ui.view.BTKeyboardView
import com.realwear.bluetooth.model.*
import kotlinx.coroutines.*

class KeyboardPresenter(private val deviceAddress: String,
                        private val view: BTKeyboardView,
                        private val scanModel: BluetoothScanner,
                        private val connectionModel: BluetoothConnector,
                        private val uiContext: CoroutineDispatcher = Dispatchers.Main,
                        private val bgContext: CoroutineDispatcher = Dispatchers.IO
) : BasePresenter() {


    private val prepareListener = object : OnPrepareListener {

        override fun onPrepared() {

            with(connectionModel) {
                addOnConnectListener(connectionListener)
                addOnMessageListener(messageListener)
            }
//            updateState()
//            dismissNotification()
        }

        override fun onError() {
            releaseConnection()
        }
    }

    private val connectionListener = object : OnConnectionListener {

        override fun onConnected(device: BluetoothDevice) {

        }

        override fun onConnectionWithdrawn() {
//            updateState()
        }

        override fun onConnectedIn(conversation: String) {
        }

        override fun onConnectedOut(conversation: String) {
        }

        override fun onConnectionDestroyed() {
//            view.showServiceDestroyed()
        }

        override fun onConnectionAccepted() {

//            view.showStatusConnected()
//            view.hideActions()

//            launch {
//                val conversation = withContext(bgContext) { conversationsStorage.getConversationByAddress(deviceAddress) }
//                if (conversation != null) {
//                    view.showPartnerName(conversation.displayName, conversation.deviceName)
//                }
//            }
        }

        override fun onConnectionRejected() {
//            view.showRejectedConnection()
//            updateState()
        }


        override fun onConnecting() {
        }

        override fun onConnectionLost() {
//            view.showLostConnection()
//            updateState()
        }

        override fun onConnectionFailed() {
//            view.showFailedConnection()
//            updateState()
        }

        override fun onDisconnected() {
//            view.showDisconnected()
//            updateState()
            view.goBack();
        }
    }

    private val messageListener = object : OnMessageListener {

        override fun onMessageReceived(message: String) {
            view.showReceivedMessage(message)
        }

        override fun onMessageSent(message: String) {
            view.showSentMessage(message)
        }

        override fun onMessageSendingFailed() {
//            view.showSendingMessageFailure()
        }

        override fun onMessageDelivered(id: Long) {
        }

        override fun onMessageNotDelivered(id: Long) {
        }

        override fun onMessageSeen(id: Long) {
        }
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun prepareConnection() {

        if (!scanModel.isBluetoothEnabled()) {
            view.showBluetoothDisabled()
        } else {

            connectionModel.addOnPrepareListener(prepareListener)

            if (connectionModel.isConnectionPrepared()) {
                with(connectionModel) {
                    addOnConnectListener(connectionListener)
                    addOnMessageListener(messageListener)
                }
//                updateState()
//                dismissNotification()
//                sendFileIfPrepared()
            } else {
                connectionModel.prepare()
            }
        }

        launch {
//            val messagesDef = async(bgContext) { messagesStorage.getMessagesByDevice(deviceAddress) }
//            val conversationDef = async(bgContext) { conversationsStorage.getConversationByAddress(deviceAddress) }
//            displayInfo(messagesDef.await(), conversationDef.await())
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun releaseConnection() {
        with(connectionModel) {
            removeOnPrepareListener(prepareListener)
            removeOnConnectListener(connectionListener)
            removeOnMessageListener(messageListener)
//            removeOnFileListener(fileListener)
        }
    }

    fun sendTextMessage(toString: String) {

        with(connectionModel) {
            connectionModel.sendMessage(toString)
        }
    }


}
