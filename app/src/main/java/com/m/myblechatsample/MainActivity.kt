package com.m.myblechatsample

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.m.myblechatsample.ui.activity.ScanActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startActivity(Intent(this, ScanActivity::class.java))
    }

    companion object {

        private const val REQUEST_ENABLE_BLUETOOTH = 101

        const val EXTRA_ADDRESS = "extra.address"
        private const val EXTRA_MESSAGE = "extra.message"
        private const val EXTRA_FILE_PATH = "extra.file_path"

        fun start(context: Context, address: String) {
            val intent: Intent = Intent(context, MainActivity::class.java)
                .putExtra(EXTRA_ADDRESS, address)
            context.startActivity(intent)
        }

        fun start(context: Context, address: String, message: String?, filePath: String?) {
            val intent: Intent = Intent(context, MainActivity::class.java)
                .setAction(Intent.ACTION_SEND)
                .putExtra(EXTRA_ADDRESS, address)
                .putExtra(EXTRA_MESSAGE, message)
                .putExtra(EXTRA_FILE_PATH, filePath)
            context.startActivity(intent)
        }
    }
}