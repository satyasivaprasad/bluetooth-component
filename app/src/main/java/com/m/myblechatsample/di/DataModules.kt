package com.m.myblechatsample.di

import com.m.myblechatsample.view.NotificationViewImpl
import com.realwear.bluetooth.ui.view.NotificationView
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module


val viewModule = module {
    single { NotificationViewImpl(androidContext()) as NotificationView }
}
