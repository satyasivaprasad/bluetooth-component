package com.realwear.bluetooth.model

import android.bluetooth.BluetoothDevice

interface BluetoothConnector {

    fun prepare()
    fun release()
    fun stop()
    fun disconnect()
    fun addOnConnectListener(listener: OnConnectionListener)
    fun addOnPrepareListener(listener: OnPrepareListener)
    fun addOnMessageListener(listener: OnMessageListener)
    fun removeOnConnectListener(listener: OnConnectionListener)
    fun removeOnPrepareListener(listener: OnPrepareListener)
    fun removeOnMessageListener(listener: OnMessageListener)
    fun connect(device: BluetoothDevice)
    fun sendMessage(messageText: String)
    fun isConnected(): Boolean
    fun isConnectedOrPending(): Boolean
    fun isPending(): Boolean
    fun isConnectionPrepared(): Boolean
    fun acceptConnection()
    fun rejectConnection()
    fun sendDisconnectRequest()
}
