package com.realwear.bluetooth.model

interface OnMessageListener {
    fun onMessageReceived(message: String)
    fun onMessageSent(message: String)
    fun onMessageSendingFailed()
    fun onMessageDelivered(id: Long)
    fun onMessageNotDelivered(id: Long)
    fun onMessageSeen(id: Long)
}