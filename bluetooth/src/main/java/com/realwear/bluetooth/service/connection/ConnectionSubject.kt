package com.realwear.bluetooth.service.connection

import android.bluetooth.BluetoothDevice

interface ConnectionSubject {

    fun handleConnectionAccepted()
    fun handleConnected(device: BluetoothDevice)
    fun handleConnectingInProgress()
    fun handleDisconnected()
    fun handleConnectionRejected()
    fun handleConnectionFailed()
    fun handleConnectionLost()
    fun handleConnectionWithdrawn()

    fun handleMessageReceived(message: String)
    fun handleMessageSent(message: String)
    fun handleMessageSendingFailed()
    fun handleMessageSeen(uid: Long)
    fun handleMessageDelivered(uid: Long)
    fun handleMessageNotDelivered(uid: Long)
    fun isAnybodyListeningForMessages(): Boolean

    fun isRunning(): Boolean
}
