package com.realwear.bluetooth.service.connection

enum class ConnectionType {
    INCOMING, OUTCOMING
}
